Pemanfaatan openAI Sebagai Customer Service untuk Menjawab FAQ Perusahaan / Organisasi melalui Platform Whatsapp


low manpower




iliteracy

FAQ (_frequently asked question_) adalah sebuah daftar dari kumpulan pertanyaan dan jawaban untuk memberikan pengetahuan umum kepada pengguna. Tujuan dari FAQ sendiri secara umum adalah untuk memberikan pengetahuan mengenai permasalahan yang sering kali ditemui oleh kebanyakan orang yang mengalami kekurangan informasi. Berkat FAQ ini, orang - orang menjadi tidak akan merasa malu atau direndahkan untuk menanyakan hal sepele (_FAQs about FAQs_, n.d.). Sehingga FAQ menjadi fitur penting untuk dalam perancangan situs web resmi khususnya untuk suatu organisasi atau perusahaan. Manfaat yang bisa didapat dengan memberikan FAQ yaitu, dapat mengurangi tenaga kerja sebagai customer service untuk membantu kesenjangan informasi pelanggan dengan informasi yang tersedia dan meningkatkan pengalaman pelanggan atau pengguna dalam mencari jawaban dari setiap topik yang mereka cari.

Namun, FAQ sendiri juga masih memiliki kekurangan. Salah satunya adalah terkadang tidak semua solusi dari suatu permasalahan dapat ditemukan pada FAQ. Sesuai dengan yang dikatakan oleh James Hupp dari akun Twitter nya, "FAQ adalah cara untuk memberikan pengetahuan yang kita pikirkan akan berguna untuk para pengguna. Namun, kita sendiri tidak mengetahui apa yang benar-benar dipikirkan oleh pengguna" (Hupp, n.d.). Meskipun begitu, bukan berarti hal tersebut tidak bisa dipecahkan. Dengan penelitian ini, diharapkan kekurangan dari FAQ tersebut dapat ditutupi dengan pemanfaatan OpenAI.

Seperti yang kita lihat, teknologi AI sekarang sangat sering kita jumpai dari berbagai macam situs dan aplikasi yang kita gunakan. Contohnya seperti chatGPT, GitHub Copilot, GrammarlyGo, Adobe Firefly, dan masih banyak lagi. Hal tersebut dikarenakan dengan mudahnya pengguna dalam membuat sesuatu ataupun menyelesaikan masalah yang spesifik dengan kasus mereka dengan hanya menggunakan AI. Sehingga dengan pemanfaatan teknologi ini, informasi yang tidak dapat ditemui pada FAQ ataupun permasalahan yang lebih spesifik dapat terselesaikan.


# TUTORIAL MEMBUAT WHATSAPP BOT OPENAI CHATGPT DARI NOL !!!
https://www.youtube.com/watch?v=TE6I24QxQVo

# Custom GUI App for Open-Assistant API - Tutorial | Step by step Guide
https://www.youtube.com/watch?v=XBVSve_SvNk