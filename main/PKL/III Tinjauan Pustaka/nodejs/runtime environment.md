# What is runtime environment (RTE)?

[#General Concepts](https://www.underthehoodlearning.com/tag/concept)

_**Runtime**_ and _**runtime environment**_ are some of the most overloaded terms in software development. I myself find it confusing. I later found out that the word _**runtime**_ itself means many different things in many different contexts. On top of that, the fact that people often time choose to use the word _**runtime**_ as short for _**runtime environment**_ just further complicates the matter. I guess I can say that my confusion is totally justified. 😛

## What is runtime?

1.  **Runtime - The Lifecycle**: The first meaning of _**runtime**_ is with regards to program lifecycle. It refers to _**the period of time**_ in which a program is _**executing**_ in contrast to the period of time in which a program is _**compiled**_. For example, we often distinguish _**compile error**_ vs. _**runtime error**_.
2.  **Runtime - How long did it take**: When used as two words, _**run time**_ refers to the raw _**execution time**_ of a program.

## What is runtime environment?

Runtime environment takes its meaning in the context of _**runtime - the lifecycle**_. Therefore, runtime environment is the environment in which a program or application is executed. It's the hardware and software infrastructure that supports the running of a particular codebase in real time. A runtime environment loads applications and has them run on a _**platform**_ - _hardware and software architecture that acts as a foundation upon which other applications, processes, or technologies are developed._ All the resources necessary for running a program independently of the _**operating system (OS)**_ are available on the platform. Practically speaking, a _**runtime environment**_ is a piece of software that is designed to run other software.

## Why do we need runtime environment?

We use a variety of computer programs every day, for tasks like photo editing, word processing, and calculation. It's expected that these programs run as fast and smoothly as possible under a variety of conditions. Since operating systems can differ significantly from each other, and even the same OS has many different versions, it's necessary for developers to adapt programs to each OS by using _**runtime environment**_.

Therefore, _**runtime environment**_ provides the following advantages:

-   _**Cross-platform functionality**_: Enables cross-platform functionality for applications, which simplifies the development process since the program does not need to be adapted to different OS.
-   _**Identical user interfaces**_: Allows programs to have identical user interfaces regardless of whether they're run on Windows, macOS, or Linux.
-   _**Conservation of resources**_: Allows similar applications to use the same runtime environments and share common components.

## How does a runtime environment work?

Your code is just code. Whatever you write, in whatever language you choose, needs to eventually execute on a computer. An application that's currently running interacts with the runtime environment via a runtime system. The _**runtime environment**_, therefore, acts as the _**middleman**_ between the application and the  operating system.

As soon as a program is executed, the _**runtime environment**_ sends instructions to the computer's processor and RAM, and accesses system resources. A _**runtime environment**_ provides various basic functions for _memory, networks, and hardware_. These functions are carried out by the _**runtime environment**_ instead of the application and work independently of the OS. They include _reading and writing files, managing input and output devices, searching and sorting files, and transporting data via networks._

> **Fun fact**: the individual modules of a runtime environment are saved in runtime libraries. In Windows, you can identify these libraries based on the extension `.dll` (dynamic link library); in Linux, they have the file suffix `.so` (shared object).

Just like programming languages, which have several layers of abstraction varying from low-level to high-level languages, _**runtime environments**_ also have their own _**layers of abstraction**_, _i.e., runtime environments have their own runtime environments._ We can look at software as a series of layers that sit on top of the system hardware. Each layer provides services that will be used and required by the layer above it.

As programming languages evolves, people wanted an environment that could handle additional tasks that felt cumbersome for developers. _Do you really enjoy using `malloc()` and `free()` to manage all your dynamic memory?_ 😉 _Wouldn't some kind of automatic reference counting and garbage collection be extremely convenient?_ This is the motivation for the development of the _**Java Runtime Environment (JRE).**_

For example, let's say your application is written in JavaScript. **JavaScript** is a high-level language whose high-level runtime environment is **Node.js**. Node comes with fancy features like _a callback queue, and event loop, and a thread pool_. As a _**runtime environment**_, Node itself has its own runtime environment. If you download the Node   binary for Linux, you'll find that it's just another ELF executable waiting to be run by the OS. So, JavaScript's runtime environment is Node, and Node's runtime environment is the operating system.

So we can say that the _**universal runtime environment**_ for any kind of programmatic execution is the _**operating system**_. The OS is the only way you can get the CPU to execute your code. The OS _ensures that your program gets sufficient memory, gets scheduled fairly, and doesn't disturbed its neighbors._ It doesn't matter if you're using **C, Python**, or **Node.js**. At the end of the day, the OS is _**everyone's runtime environment**_.