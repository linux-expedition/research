##### linting
https://www.perforce.com/blog/qac/what-lint-code-and-what-linting-and-why-linting-important
##### eslnt
https://en.wikipedia.org/wiki/ESLint
wiki
**ESLint** is a [static code analysis](https://en.wikipedia.org/wiki/Static_code_analysis "Static code analysis") tool for identifying problematic patterns found in [JavaScript](https://en.wikipedia.org/wiki/JavaScript "JavaScript") code. It was created by Nicholas C. Zakas in 2013.[[2]](https://en.wikipedia.org/wiki/ESLint#cite_note-2)[[3]](https://en.wikipedia.org/wiki/ESLint#cite_note-:1-3) Rules in ESLint are configurable, and customized rules can be defined and loaded. ESLint covers both [code quality](https://en.wikipedia.org/wiki/Coding_conventions "Coding conventions") and [coding style](https://en.wikipedia.org/wiki/Programming_style "Programming style") issues. ESLint supports current standards of [ECMAScript](https://en.wikipedia.org/wiki/ECMAScript "ECMAScript"), and experimental syntax from drafts for future standards. Code using [JSX](https://en.wikipedia.org/wiki/JSX_(JavaScript) "JSX (JavaScript)") or [TypeScript](https://en.wikipedia.org/wiki/TypeScript "TypeScript") can also be processed when a plugin or transpiler is used.[[4]](https://en.wikipedia.org/wiki/ESLint#cite_note-4)[[5]](https://en.wikipedia.org/wiki/ESLint#cite_note-5)
##### jest
https://jestjs.io/
