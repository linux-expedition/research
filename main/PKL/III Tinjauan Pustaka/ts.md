Typescript adalah bahasa pemrograman bersifat open source yang syntaxnya menyerupai dengan JavaScript. Dikembangkan oleh Microsoft, bahasa pemrograman ini memang diciptakan berdasarkan bahasa JavaScript (wiki ped definition first footnote) Di mana merupakan superset dari JavaScript dengan tambahan static typing di dalamnya. Jadi, penulisan bahasa TypeScript sangat mirip dengan JavaScript. Namun tidak seperti JavaScript yang hampir semua datanya dapat dilempar ke mana saja tanpa mendefinisikan tipe datanya, TypeScript berlaku sebaliknya. TypeScript memberikan kemampuan para developer untuk dapat mendefinisikan secara langsung tipe data dari setiap variable, parameter fungsi, dan bahkan return type yang ada pada kode. 

**The biggest feature that TypeScript brings to the table is static types.**
https://dev.to/nicozerpa/typescript-why-static-typing-matters-444d#:~:text=If%20you%20have%20to%20refactor,for%20humans%20and%20for%20robots!

Hal tersebut memberikan kemampuan IDE untuk memberi tahu kepada developer bahwa ada error di dalam kode. Sehingga developer dapat mengetahui error tersebut terlebih dahulu sebelum program dijalankan. Serta menghindari proses debugging satu demi stau untuk mencari kesalahan pada kode. Kode dengan tipe data yang sudah terdefinisi juga membantu antar developer untuk saling memahami perilaku kode. Kode pun dapat menjelaskan dirinya sendiri tanpa membutuhkan komentar maupun orang lain untuk menjelaskannya Dengan data dengan tipe data yang terdefinisikan dengan jelas di dalam kode, Hal tersebut memberikan 
sendiri adalah superset yang sama dengan JavaScript, namun dengan terdapat tambahan static typing di dalamnya. Memang secara bahasa pemrograman memang beda dengan JavaScript. Di mana TypeScript ada

#####  i     
TypeScript is an open-source programming language developed by Microsoft. It is a statically typed superset of JavaScript, which means it extends the capabilities of JavaScript by adding static typing to it. TypeScript compiles to plain JavaScript and can be executed in any JavaScript runtime environment, such as web browsers or Node.js.

Here are some key features and concepts of TypeScript:

1. **Static Typing**: TypeScript introduces static typing to JavaScript. This means that variables, function parameters, and return types can have explicit types defined. This helps catch type-related errors during the development phase, allowing developers to write more reliable code.

2. **Type Inference**: TypeScript has a powerful type inference system that can infer the types of variables based on their initial values. This reduces the need for explicit type annotations and makes the code more concise and readable.

3. **Interfaces and Classes**: TypeScript supports the definition of interfaces and classes. Interfaces allow you to define the structure of an object, specifying the properties and methods it should have. Classes provide a way to create objects based on a blueprint, enabling object-oriented programming concepts like inheritance, encapsulation, and abstraction.

4. **Generics**: TypeScript includes support for generics, which allow you to define reusable components that can work with different types. Generics add flexibility to functions, classes, and interfaces, enabling code reusability and type safety.

5. **Module Support**: TypeScript supports modern module systems such as CommonJS, AMD, and ES modules. Modules help organize code into separate files, making it easier to manage and reuse code across different parts of an application.

6. **Type Annotations and Type Checking**: TypeScript provides a rich set of type annotations that can be used to describe the shape and behavior of variables, functions, and objects. The TypeScript compiler performs static type checking to detect type errors and provides feedback during development, helping catch potential issues early on.

7. **Tooling and Editor Support**: TypeScript integrates well with popular code editors and provides features like autocompletion, code navigation, and refactoring tools. It enhances the development experience by providing better tooling and error reporting compared to plain JavaScript.

By introducing static typing and additional features, TypeScript aims to improve the development experience, reduce bugs, and make large-scale JavaScript projects more maintainable and scalable. It is widely adopted in the industry and is used in many popular frameworks and libraries such as Angular, React, and Express.js.
##### W3
###### What is TypeScript?

TypeScript is a syntactic superset of JavaScript which adds **static typing**.

This basically means that TypeScript adds syntax on top of JavaScript, allowing developers to add **types**.
###### Why should I use TypeScript?

JavaScript is a loosely typed language. It can be difficult to understand what types of data are being passed around in JavaScript.

In JavaScript, function parameters and variables don't have any information! So developers need to look at documentation, or guess based on the implementation.

TypeScript allows specifying the types of data being passed around within the code, and has the ability to report errors when the types don't match.

For example, TypeScript will report an error when passing a string into a function that expects a number. JavaScript will not.
###### Why should I use TypeScript?

JavaScript is a loosely typed language. It can be difficult to understand what types of data are being passed around in JavaScript.

In JavaScript, function parameters and variables don't have any information! So developers need to look at documentation, or guess based on the implementation.

TypeScript allows specifying the types of data being passed around within the code, and has the ability to report errors when the types don't match.

For example, TypeScript will report an error when passing a string into a function that expects a number. JavaScript will not.
##### official
###### What is TypeScript?
###### JavaScript and More
TypeScript adds additional syntax to JavaScript to support a **tighter integration with your editor**. Catch errors early in your editor.
###### A Result You Can Trust
TypeScript code converts to JavaScript, which **runs anywhere JavaScript runs**: In a browser, on Node.js or Deno and in your apps.
###### Safety at Scale
TypeScript understands JavaScript and uses **type inference to give you great tooling** without additional code.
###### Adopt TypeScript Gradually
Apply types to your JavaScript project incrementally, **each step improves editor support** and improves your codebase.
Let's take this incorrect JavaScript code, and see how **TypeScript can catch mistakes in your editor**.