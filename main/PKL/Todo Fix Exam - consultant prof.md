Problem
	Jadwal masih bisa dibuka dan diklik mulai, ketika jadwal sudah melewati batas expired
	Diklik mulai hanya refresh halaman tanpa notifikasi error
Expectation
	Tidak bisa diklik mulai atau tidak bisa dibuka
Step to do it
	Buat jadwal dengan deadline waktu dekat
	Tidak dikerjakan sampai melewati deadline
	Buka jadwal melalui Top Highlight Jadwal warna Biru



Jan 31
Konsultasi alur pembuatan surat pengantar

Feb 2
Perizinan untuk kegiatan club GDSC
Pemberian informasi jam kerja, pergantian nama perusahaan,
Proses pembimbingan kedepannya

Feb 10 
Pengambilan KRS dan konsultasi mata kuliah yang akan dikonversi konversi

Feb 14
Konfirmasi permohonan perizinan untuk mengisi kegiatan di club GDSC
Pemastian kebutuhan kelangkapan dokumen yang diperlukan untuk magang

Maret 1
Konfirmasi dokumen PKL
Konsultasi untuk kegiatan club mengenai solution challenge

Maret 3
Konfirmasi validasi PKL

Maret 31
Konsultasi prosedur perizinan magang untuk mengikuti seminar

April 4
Konfirmasi pembuatan surat pengantar perusahaan

Mei 15
Konsultasi isi dari laporan PKL magang

Mei 30
Konsultasi pergantian akhir magang pada surat pengantar

