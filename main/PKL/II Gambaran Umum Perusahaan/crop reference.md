PT company, logo, identitas, portofolio/client

**perusahaan**: PT. Thunder Labs Indonesia adalah perusahaan yang bergerak di bidang industri pengembangan aplikasi (software development). Perusahaan ini memiliki tujuan yaitu membangun aplikasi berdasarkan tujuan dan kriteria-kriteria yang dibutuhkan oleh client. Sehingga perusahaan ini akan selalu terlibat pada setiap tahap pengembangannya mulai dari penentuan cakupan permasalahan aplikasi, perencanaan, sampai kebijakan, dan konsekuensi penggunaan aplikasi. 
Layanan yang diberikan oleh perusahaan PT.... berupa pembuatan aplikasi berbasis web dan mobile secara cross platform dan juga IT consulting. Pada setiap proses pengembangannya, PT. Thunder Labs Indonesia akan selalu melibatkan client-nya. Agar terciptanya aplikasi dengan kualitas yang terbaik, transparasi akan menjadi salah satu komitmen yang selalu dijaga oleh PT. Thunder Labs Indonesia. Dengan begitu, kejelasan konsekuensi dari suatu kebijakan akan tercapai dengan hasil terbaik. Perusahaan PT. Thunder Labs Indonesia juga telah memiliki berbagai macam portofolio di antaranya yaitu, Kontena (manajemen hotel), SkyTech(sitem retail store), Gant Rugi(sistem ansuransi), gokredit.com(sistem banking), Reliance(sistem human resources), dan masih banyak lagi.


Dengan komitmen melibatkan client dalam segala proses pembuatan aplikasi. Transparasi dari kedua belah pihak akan terjamin

**Identitas**:..
Nama Instansi : PT Thunder Labs Indonesia
No SK : AHU-0027339.AH.01.02.TAHUN 2017
Tanggal SK : 22 Desember 2017
Jenis Perusahaan : Industri
Email : [hello@thunderlab.id](mailto:hello@thunderlab.id)
Kontak : (0341) 4376866
Jam Kerja
Senin – Jum’at : Pukul 08.00 – 16.00
Sabtu : Pukul 08.00 – 13.00

**Logo**: Logo PT. ... memiliki desain dengan karakteristik yang menyerupai dengan namanya. Hal tersebut bisa dilihat dalam pandangan pertama. Dapat segera dikenali bahwa adanya conical flask atau bentuk gelas berbentuk kerucut yang biasa ditemui di lab. Di mana diwakili oleh nama perusahaannya yaitu Labs. Begitu juga dengan nama Thunder. Di mana dapat ditemui simbol petir pada gagang gelas lab tersebut.

###### portfolio
kontena(manajemen hotel), skytech(sistem retail store), ganti rugi (sistem ansuransi), gokredit.com (sistem banking), reliance (sistem human resources)

alamat
Alamat Perusahaan : Jl. Letjen Sutoyo No.102, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur
- [Vernon Building Floor 2  
    Jalan Letjen Sutoyo No.102, Bunulrejo, Blimbing, Kota Malang, Jawa Timur 65141](https://goo.gl/maps/4KMHX99AsZK9dPHz8)

###### team
Pengembangan aplikasi CBT SMPN 13 Malang dikerjakan sebagai tim. Dengan personel berjumlah 5 orang termasuk penulis. PT Thunder Labs Indonesia menerapkan sistem kerja bersifat agile. Maka dari itu, bidang pekerjaan yang diambil oleh setiap personel selalu berganti-ganti tergantung dari kebutuhan pada saat itu.
Implementator
Seseorang yang berposisi sebagai implementator, memiliki tanggung jawab menjadi sebagai technical support dalam membantu client. Apapun kendala atau hal yang tidak diketahui oleh client, akan berhubungan langsung dengan implementator sebagai first responden. Maka dari itu, mengetahui keselurahan design dan struktur aplikasi merupakan kewajiban seorang implementator untuk memahaminya. Contoh tugas dari implementator seperti memberikan pelatihan, pendampingan, dan memberikan solusi terhadap persoalan-persoalan yang ditemui oleh user ketika menggunakan aplikasi.
project manager
Setiap project pasti membutuhkan sumber daya untuk memastikan agar proyek tersebut terselesaikan dengan baik. Sumber daya tersebut meliputi waktu, orang, peralatan, bahan, dan seterusnya. Agar semua sumber daya tersebut terorganisir dengan baik dibutuhkannya seorang project manager. Bertugas untuk menyederhanakan proses pengerjaan dan mengorganisir sumber daya proyek. Contoh sederhananya seperti merekrut anggota tim, menentukan  jadwal proyek, memberikan tugas ke tim, dan bahkan pada proses pemantuan dan pengendalian proyek juga tugas dari project manager. Karena keputusan-keputusan atau pengendalian proyek secara proses maupun arah tujuan adalah bagian dari keseharian yang dilakukan oleh seorang project manager. Maka dari itu, project manager merupakan posisi krusial yang harus diisi. Sehingga tidak heran jika seorang project manager memiliki tanggung jawab yang besar terhadap keberlangsungan proyek tersebut. 
Penyedia API
Tugas dari penyedia API adalah melakukan manajemen, maintenance, dan pengembangan API. Hal tersebut dilakukan agar setiap aplikasi yang memiliki keterhubungan dengan data terkait dapat berkomunikasi dan saling bertukar data secara efektif dan efisien. Contoh spesifiknya yaitu, penyedia API juga akan terlibat secara langsung dalam proses analisa performa API, memberikan support kepada developer mengenai hal alur yang tidak dipahami, mendesain struktur API agar sesuai best practice dan standard industry, dan juga memastikan keamanan untuk menjaga data sensitif seperti authentication dan authorization. Sehingga dengan adanya penyedia API, proses integrasi dan pertukaran data dapat dilakukan secara halus. Posisi penyedia API juga berperan penting dalam memastikan keberhasilan pertumbuhan ekosistem dari sebuah aplikasi. Memberikan kemudahan bagi para developer untuk membangun aplikasi yang menarik dan inovatif di atas API yang sudah dibuat.
Pengembangan website admin
Aplikasi yang terlibat pada proses pengembangan maupun pengaplikasian ujian CBT ada 2 yaitu, website admin dan aplikasi ujian. Pengembangan ini dibagi dua agar setiap pengerjaannya memiliki fokus dengan posisinya masing-masing pada saat itu. Untuk yang pertama yaitu Website admin, akan menjadi induk aplikasi ujian. Maka dari itu, proses pengerjaan website admin akan dilakukan terlebih dahulu sebelum menyelesaikan aplikasi ujian untuk siswa. Pada orang yang terlibat pada posisi ini akan bertanggung jawab untuk mengimplementasikan semua permintaan fitur dari guru. Di mana fitur-fitur yang diinginkan seperti manajemen siswa, jadwal, soal, guru pengawas, ruang kelas, kecurangan siswa, dan sesi ujian.
Pengembangan aplikasi ujian
Aplikasi ujian siswa seperti perpanjangan dari aplikasi website admin. Sehingga para siswa tidak akan bisa melakukan apapun kecuali para guru dapat melakukan manajemen ujian dengan baik. Maka dari itu, aplikasi ujian ada setelah website admin telah stabil untuk digunakan. Pengembang yang berposisi sebagai pengembang aplikasi ujian akan memiliki berbagai macam tanggung jawab untuk dipenuhi. Di antaranya yaitu, mengimplementasi seluruh fitur dan kebijakan yang dikeluarkan oleh guru, memastikan para siswa dapat mengerjakan secara kondusif, menampilkan soal guru secara akurat, dan mencegah kecurangan siswa.
###### original
## Our **Cores**

#### Thunderlab menawarkan solusi untuk bisnis Anda dengan penggunaan teknologi secara maksimal.

Kami berkomitmen untuk selalu ada dipihak Anda. Kami percaya bisnis yang lestari adalah bisnis yang dapat mengendalikan arus keuangan. Dengan platform teknologi, kami siap membantu bisnis Anda mencapai misi ini.

Kami berkomitmen untuk selalu melibatkan Anda. Kami percaya teknologi hanya berguna jika memiliki tujuan yang jelas. Dengan demikian Andalah yang paling pantas menjadi supervisor bagi pengembangan teknologi untuk bisnis Anda.

Kami berkomitmen untuk selalu menghargai transparasi. Kami percaya kejelasan konsekuesi dari suatu kebijakan akan menghasilkan keputusan yang berkualitas. Anda akan mendapat penjelasan tentang konsekuensi kebijakan dalam pengembangan teknologi untuk bisnis Anda.


###### ongkowidjojo
PT. Ongkowidjojo adalah perusahaan yang bergerak di bidang Industri hasi tembakau, yang didirikan pada tahun 1946 oleh Bapak Ong Kian dan Bapak Liem Tjiang Gie. Saat itu PT. Ongkowidjojo masih berbetuk firma dengan nama Firma Kian Gie. Perusahaan ini terletak di jalan Kolonel Sugiono No. 28 Malang. Namun, karena adanya agresi militer Belanda pada tahun 1948 semua kegiatan produksi dan administrasi dipindahkan di Jalan Halmahera No. 74 Malang, kemudian disusul dengan adanya pendirian gudang di jalan Gilintung No. 49 Malang. Dikarenakan tidak mendapatkan izin pendirian di Jalan Halmahera No. 74 Malang, maka kegiatan produksi dan administrasi di Jalan Halmahera No. 74 Malang dipindahkan ke jalan Kolonel Sugiono No. 28 Malang


PT Andromedia adalah perusahaan yang bergerak di bidang IT consulting, software development, dan implementasi ERP (Enterprise Resource Planning) dengan karyawan berjumlah kurang lebih sebanyak 64 karyawan yang bekerja secara full time. PT Andromedia didirikan pada tahun 2008 oleh A. Ghofur yang merupakan alumni mahasiswa jurusan Sistem Informasi dari Universitas Institut Teknologi Sepuluh Nopember (ITS) tahun 2001. Perusahaan ini terletak di Jl. Jemur Andayani XV No.3B, Jemur Wonosari, Kec. Wonocolo, Kota SBY, Jawa Timur 60237. Kemudian, dapat dilihat pada gambar 2.1 merupakan logo perusahaan PT Andromedia yang didesain menyerupai bentuk huruf “A” dengan arti “Andromedia” yang membuat logo ini terkesan identik dengan indentitas perusahaan.

Untuk meningkatkan dan memungkinkan solusi sistem sebuah perusahaan untuk mengatasi perubahan proses bisnis, PT Andromedia menawarkan berbagai solusi yang disesuaikan untuk industri besar, nasional, dan multinasional yang beragam seperti IT Business Plan, Master Plan & Architecture, Monitoring, Service Management, dan Project Management. PT. Andromedia telah dipercayai oleh segenap klien-kliennya seperti Citilink, Kemkominfo, Pertamina EP, Semen Indonesia, dan Wings.

#### Falac
2.4 Deskripsi Lingkup Kerja Full-Stack Developer 
Pelaksanaan program magang di PT Andromedia, dimulai pada tanggal 18 Agustus 2022 - 31 Desember 2022. Jadwal kegiatan magang adalah hari seninjumat mulai pukul 09.00 WIB - 16.00 WIB. Berikut merupakan timeline kegiatan magang di PT Andromedia: 
2.4.1 On Boarding (18 Agustus – 19 Agustus 2022) On Boarding diselenggarakan oleh pihak Kampus Merdeka untuk pelepasan peserta angkatan 2 dan penyambutan angkatan 3. 
2.4.2 Pembekalan Materi dan Mini Challenge (22 Agustus – 30 September 2022) Melakukan pengerjaan mini challenge yang telah diberikan oleh mentor untuk pelatihan skill selama kurang lebih satu bulan dengan pendampingan mentor. Target pencapaian adalah penyelesain mini challenge yang telah diberikan dan pemahaman konsep library React.js, Framework Tailwind CSS, dan OpenCv.js. 
2.4.3 Pembekalan Mini Project (03 Oktober – 21 Oktober 2022) Melakukan cloning project salah satu klien PT Andromedia yang berbasis website. Pengembangan website dilakukan dengan menggunakan framework Royal dan OpenCv.js. 
2.4.4 Project Magang Full-Stack Developer (24 Oktober – 31 Desember 2022) Full-Stack Developer bertugas untuk mengerjakan front-end maupun back-end dari aplikasi yang dikembangkan, dan memastikan apakah tampilan aplikasi yang dibuat sudah memenuhi kebutuhan klien. Kegiatan ini merupakan implementasi materi secara nyata dengan project yang sudah ditentukan oleh mentor yang dikerjakan peserta magang khususnya Tim Full-Stack Developer. Peserta magang diminta untuk mengerjakan beberapa task seperti, membuat fitur tambahan, merubah design, dan perbaikan bug pada bagian project yang sedang dikerjakan. Selama pengerjaan project akan didampingi oleh mentor dari divisi Full-Stack Developer dan Business Analyst.

#### Own data

Seiring dengan berjalannya zaman, meningkat juga keperluan teknologi sebagai permudah


Perusahaan PT. Thunder Labs Indonesia ini terletak di jalan Letjen Sutoyo No.102, Kecamatan Blimbing, Kota Malang.

l. Letjen Sutoyo No.102, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur

menawarkan solusi untuk bisnis Anda dengan penggunaan teknologi secara maksimal.

- Kami berkomitmen untuk selalu ada dipihak Anda. Kami percaya bisnis yang lestari adalah bisnis yang dapat mengendalikan arus keuangan. Dengan platform teknologi, kami siap membantu bisnis Anda mencapai misi ini.

 Kami berkomitmen untuk selalu melibatkan Anda. Kami percaya teknologi hanya berguna jika memiliki tujuan yang jelas. Dengan demikian Andalah yang paling pantas menjadi supervisor bagi pengembangan teknologi untuk bisnis Anda.

Kami berkomitmen untuk selalu menghargai transparasi. Kami percaya kejelasan konsekuesi dari suatu kebijakan akan menghasilkan keputusan yang berkualitas. Anda akan mendapat penjelasan tentang konsekuensi kebijakan dalam pengembangan teknologi untuk bisnis Anda.

Partner Teknologi untuk Pebisnis

We care most about cashflow  
Spend less, Profit more
