[[head]]
[[timeline]]

###### in
Kegiatan Praktik Kerja Lapangan (PKL) pada PT...  yang dilaksanakan oleh penulis, berlangsung selama 6 bulan. Kegiatan tersebut juga dijadikan satu dengan magang atau program internship yang dilaksanakan selama 6 bulan. Awal penulis melaksanakan magang, terhitung mulai pada tanggal 1 Februari 2023 sampai 31 Juli 2023.

Perusahaan PT... , memiliki beberapa aturan yang harus dipatuhi oleh karyawanya maupun bagi para intern. Serta sebagai mahasiswa yang membawa almamater universitas, tentu wajib bisa beradaptasi dengan semua etika maupun peraturan yang telah diterapkan oleh pihak perusahaan.  Peraturan tersebut diantaranya adalah adanya 6 hari kerja dalam seminggu. Di mana jam kerjanya dimulai dari jam 8 - 4 sore. Sedangkan untuk hari sabtu, dimulai dari jam 8 - 1 siang. Lalu bagi para intern, setiap hari diwajibkan untuk memberikan progress report sebagai bukti kehadiran setiap harinya pada aplikasi Discord dengan server yang telah disediakan.

Kegiatan PKL dimulai dengan belajar react native. Proses belajar yang diberikan adalah bersifat auto didak. Namun, penulis tentu diberikan kesempatan bertanya kepada para developer dari perusahaan apabila menemui hal yang kurang dimengerti. Dengan pembelajaran yang bersifat self explore, tingkat keberhasilan penulis dalam menguasai react native ditentukan dari keaktifan dan pemahaman dalam menyerap materi yang ada. 

Pemahaman terhadap react native tersebut diperlukan untuk memberikan pengetahuan dasar pada teknologi yang akan digunakan pada tahap selanjutnya. Di mana tahap selanjutnya yang diberikan oleh pihak perusahaan untuk dipelajari adalah aplikasi DraftBit. Sebagai awal penggunaan aplikasi DraftBit, penulis diperbolehkan untuk belajar dan bereksperimen dengan akun sendiri atau pun akun yang telah disediakan.

###### proses pengerjaan
Perusahaan PT. memiliki project yang sedang berjalan dan membuka para intern untuk bergabung di dalam project tersebut. Salah satu project yang sedang dikembangkan oleh perusahaan PT adalah Akademika. Project Akademika adalah software untuk membantu para guru dalam memanajemen para siswa maupun sekolahnya. Sedangkan penulis terlibat pada pengembangan aplikasi Computer Based Test (CBT) pada SMPN 13 Malang dari project akademika.

Agar dapat mengembangkan aplikasi ... sesuai dengan yang diharapkan, dalam prosesnya terdapat beberapa tahap yang perlu dilalui. Tahapan  tersebut bisa disebut dengan System Development Life Cycle di mana diantaranya yaitu, analisis kebutuhan, desain, implementasi, pengujian, rilis produk, dan pemeliharaan. Dengan menerapkan SDLC, perusahaan dapat memberikan produk berkualitas tinggi. Karena di setiap tahapnya, aplikasi akan selalu  terpantau, terawat, dan bahkan mengalami peningkatan berkat dari setiap fase yang dilakukan secara berurutan. Agar dapat memahami alur perjalanan hidup aplikasi yang akan dikembangkan, berikut adalah diagram dari setiap proses.

diagram

discussion , 
start ---> pilih jadwal ---> start sesi ujian pada jadwal ---> kerja soal ---> finish up ---> done

Aplikasi DraftBit digunakan karena keunggulannya dalam membuat aplikasi secara instant tanpa membuat code dari awal. Sehingga dengan keunggulannya tersebut, diharapkan penulis dapat belajar dari materi react native sebelumnya. Serta penulis dapat sekaligus menerapkannya ke dalam design aplikasi dengan DraftBit yang berbasis GUI. 

######  analisa kebutuhan
Sebelum membuat aplikasi CBT dengan DraftBit, tahap pertama yang harus dilalui oleh penulis terlebih dahulu adalah analisis kebutuhan. Dengan melalui tahap ini terlebih dahulu, penulis dapat mengetahui gambaran besar dari aplikasi dari segi alur maupun kebutuhan sistem. Pada tahap ini, penulis memerlukan banyak waktu dalam memahami sistem aplikasi secara menyeluruh. Sehingga dalam prosesnya ketika penulis menemukan hal yang tidak bisa dipahami, akan diperlukan waktu untuk berdiskusi dengan pembimbing lapangan agar fitur dan alur sesuai dengan perencanaan. 
Terdapat beberapa informasi yang perlu diketahui pada tahap ini. Yang pertama yaitu, telah dibuatnya dashboard admin untuk para guru atau pengawas ujian dalam memanajemen dan mengawasi siswa ujian. Sehingga kemunculan jadwal ujian pada aplikasi CBT adalah berasal dari guru yang menciptakan jadwalnya di tempat pertama. 
Kedua, hampir semua aktifitas yang dilakukan oleh siswa pada aplikasi CBT dapat terekam oleh website dashboard admin. Hal tersebut bukan hanya sekedar penilaian saja. Kemampuan dalam mendeteksi kecurangan sangat diperlukan oleh para guru dalam menilai siswa secara objektif dan akurat.

Diagram yang dapat membantu memahami flow siswa dan keterlibatan guru pada aplikasi tersebut.

##### perancangan aplikasi
Pada saat memasuki tahap ini, penulis telah diberikan design aplikasi. Sehingga pada tahap ini penulis hanya melakukan implementasi fitur dan design ke bentuk aplikasi. Pada tahap ini pun penulis mulai menggunakan aplikasi DraftBit di mana merupakan awal kali penulis melakukan code atau memulai pembuatan aplikasi. 
###### Authentification
Pada alur penggunaan aplikasi CBT ini, diawali dengan sistem otentikasi siswa. Para siswa akan diberikan 2 halaman yaitu, halaman login dan halaman lupa password. Halaman login hanya bisa diisi oleh siswa yang telah menerima akun dari guru.  Seperti yang terlihat pada gambar!!!, siswa akan menerima password dari email yang telah diberikan. Sedangkan halaman reset password adalah fitur yang dapat digunakan ketika siswa melupakan password yang diberikan. Siswa juga dapat membuat passwordnya sendiri dengan menggunakan fitur lupa password. Fitur tersebut hanya bisa digunakan apabila siswa mengetahui emailnya masing-masing.
###### Jadwal
Halaman daftar jadwal adalah halaman beranda aplikasi CBT setelah melakukan otentikasi. Halaman ini memiliki beberapa fitur di dalamnya. Terdapat pengecekan otentikasi pengguna yang digunakan untuk menampilkan nama pengguna. Bahkan peringkat jadwal menempati paling atas pun juga memiliki peraturan yang telah ditetapkan. Jadwal teratas adalah jadwal yang telah dibuka dan sedang berlangsung. Sedangkan jadwal yang telah berakhir akan disortir lagi berdasarkan yang terbaru. Jadi, jadwal yang paling bawah adalah jadwal yang telah selesai pada waktu yang paling lampau.
Menekan pada salah satu jadwal akan mengarahkan siswa ke halaman selanjutnya yaitu, halaman detail jadwal. Pada halaman detail jadwal yang masih terbuka, siswa akan diberikan pilihan untuk menekan tombol mulai atau kembali. Segala aktifitas siswa akan tercatat oleh sistem pada saat tombol mulai ditekan. Di mana penekanan tombol tersebut menandakan bahwa siswa telah memulai ujian. Sehingga tombol tersebut akan mengarahkan siswa ke halaman berikutnya yaitu, halaman daftar ujian.
######  Ujian
Pertama kali para siswa memasuki ujian, para siswa akan diperlihatkan daftar soal ujian terlebih dahulu. Halaman ini memiliki fitur untuk dapat memberikan informasi kepada siswa bahwa soal telah terjawab atau belum. Dengan informasi tersebut siswa akan lebih berhati-hati ketika mengirimkan lembar jawabannya. Di mana tombol kirim tersebut terletak pada bagian bawah halaman daftar ujian. Ketika tombol kirim di tekan, akan memunculkan pesan konfirmasi. Sehingga untuk mengirimkan jawaban ujian, siswa perlu melakukan upaya menekan tombol 2 kali yaitu, tombol kirim dan tombol ya dari pesan konfirmasi yang muncul. Adanya kemunculan tersebut agar tidak terjadi kesalahan apabila tertekan tanpa disengaja.
Setiap soal yang muncul pada halaman daftar soal ini merupakan navigasi menuju halaman soal. Sehingga menekan salah satu diantaranya akan mengarahkan siswa ke halaman soal yang telah ditekan. Halaman ini memiliki beberapa navigasi. Navigasi yang dimiliki oleh halaman ini itu diantaranya, soal selanjutnya, soal sebelumnya, dan kembali ke daftar soal. Untuk jenis soal yang akan muncul pada halaman soal ini yaitu, pilihan ganda dan menjodohkan.

###### Hasil
Setelah selesai mengerjakan ujian dan telah mengirimkan jawaban, siswa akan diarahkan ke halaman hasil. Nilai dari soal yang telah dijawab akan langsung muncul pada halaman ini. Hasil ujian juga akan tersimpan otomatis pada website dashboard para guru. Sehingga para guru tidak perlu melakukan koreksi jawaban secara manual. Halaman ini hanya memiliki satu tombol navigasi yaitu kembali ke halaman beranda siswa atau daftar jadwal.
Implementasi feature & design  = auth, home, screens...
Demo Day = (any words about what demo is and what you'll do on the demo session) Installing exam browser
Demo
Setelah aplikasi sudah siap untuk digunakan, aplikasi terlebih dahulu akan di demokan terlebih dahulu kepada guru sebelum diimplementasikan ke sistem sekolah.
Kendala dan hambatan
	Berdasarkan dari hasil demo dari bab di atas. Telah didapati beberapa kendala dan masalah. Di antaranya permasalahan atau bug yang ditemui adalaha sebagai berikut
	Keamanan....
		There's still flaw and false flag detected
	performance ....
		Device
			add check device
		Signal
			upgrade or fixing dns resolver and api server akademika
		Apps
			remove redundant design
			remove unused library
			update version to newer
			new project, not from draftbit -> from template with latest version
			introduce ui kitten
			check apps version
	Policy.....
		remove hasil screen, 
		login no password - email changed to no peserta === remove reset password
Peningkatan Aplikasi di Versi 2
.....
Otentikasi
....

Standard apps = 
	design (UX), 
	performance 
		(Device ||  User
		DNS RESOLVE, API RESPONSE, Lag on Apps), 
	keamanan
##### Umpan balik


Upgrading to V2
	teknis => app (device, signal) || policy


positif
	layouting GUI
	easy to fetch API
	custom function
	can be exported to project even if trial
negative
	trial, limited time using draftbit
	lack of nested conditional feature
	it's new on its kind, a lot of performance issue or use the feature because lack of question on forum nor guideline
	limited flow because of lack of nested condition




###### dev
Keterlibatan penulis pada project yang paling awal adalah dimulai dengan merubah design ke aplikasi react native menggunakan DraftBit. Namun, sebelum memulai mengaplikasikan hasil design yang diberikan, penulis perlu untuk memahami flow design tersebut terlebih dahulu.

Aplikasi 

proses design pun lebih cepat dar Begitu juga dengan fitur preview yang memudahkan penulis dalam men-design aplikasi yang konsisten untuk tampilan web, android, maupun ios.

Pengerjaan design 
