February 1, 2023
setup dan running react - clone project example sebagai bahan belajar
February 2, 2023
 - Dasar design dengan draftbit
February 3, 2023
- Design 4 screen, 10 screen left - Selesai API login 
February 6, 2023
- Design 11 screen, 2 screen left - Merapikan design lama
February 7, 2023
- Design screen ke 12 selesai - Belajar custom component
February 8 2023
- Buat custom component untuk progress bar - Implement progress bar statis di satu screen 
February 9, 2023
- Buat custom component untuk shadow - Replace component surface di 3 screen dengan custom shadow
February 10, 2023
- Memerbaiki auth screen untuk keyboard aware - Merapikan layout untuk auth screen
February 11, 2023
- Implement API dan memperbaiki tampilan 
February 13, 2023
- Belajar custom component dengan parameter untuk htmlView - Belajar refresh screen dengan navigation atau refetch
February 14, 2023
- Memperbaiki screen ujian - Set up react native
February 15, 2023
- Clone draftbit project
February 16, 2023
- Ganti API dan re-sync data - Belajar react nativ 
February 17, 2023
- Memperbaiki navigasi parameter - Memperbaiki design screen ujian
February 20, 2023
- Menambahkan soal bertipe menjodohkan - Menyelesaikan submit jawaban per soal pada semua tipe
February 21, 2023
- Demo aplikasi - Belajar mengenai kekurangan dan tambahan kedepannya aplikasi
February 22, 2023
- Fix font tag ketika render html - Fix request list soal
February 23, 2023
- Foto untuk pilihan ganda - Foto untuk pertanyaan - Fix data menjodohkan dari API
February 24, 2023
- Menambahkan check duplicate menjodohkan - Mendeteksi kosong array di lembar soal - Menambahkan loading state di soal screen
February 26, 2023
- Menambahkan show/hide navigasi soal - Memperbaiki loading state soal
February 27, 2023
- Menjodohkan gambar - Pull to refresh lembar soal - Error handler di gambar soal
February 28, 2023
- Demo aplikasi
March 2, 2023
- Membuat dokumentasi penggunaan aplikasi pada platform android
March 3, 2023
- Belajar react native - Belajar solid principle OOP di Java
March 9, 2023
- Listing modul workshop android - Solving merge conflict versi production
March 10, 2023
- Belajar object tracking dengan opencv
March 11, 2023
- Belajar mengukur object dengan euclidean distance
March 13, 2023
- Mengawasi ujian - Masih belajar lagi opencv
March 14, 2023
- Mengawasi ujian - Membuat panduan dari feedback
March 15, 2023
- Mengawasi ujian - Initialize react native expo baru
March 16, 2023
- Instalasi ujian di PC - Init beberapa template react native
March 17, 2023
- Belajar guideline project expo react native - Mencari plugin dan navigasi handler
March 18, 2023
- Membuat modul workshop kotlin
March 20, 2023
- Design authentication setengah jadi
March 21, 2023
    - Menyelesaikan Auth Screen
    - Mengganti warna tema
March 24, 2023
- Design Home screen
March 25, 2023
    - Menambahkan custom component action bar untuk semua screen
    - Menyelesaikan Home screen
March 27, 2023
    - Memperbaiki home screen switch dan flatscreen
March 28, 2023
    - Menambahkan warning di action bar
    - Menambahkan modal alert
March 29, 2023
    - Memperbaiki action bar
    - Buat custom component text html
    - Tambah custom loading dan button
March 30, 2023
    - Tambah exam screen
    - Button refresh pada detail exam
March 31, 2023
    - Bug testing jadwal
    - Memperbaiki exam screen
April 1, 2023
    - Desain halaman soal
April 3, 2023
    - Merubah loading screen
    - Mempelajari halaman list soal dan detail soal
April 4, 2023
    - Memperbaiki tampilan kartu kosong
    - Memperbaiki loading di Home screen
April 5, 2023
    - Mempelajari perubahan soal
April 8, 2023
    - Bug testing aplikasi
April 10, 2023
    - Memperbaiki alert untuk web
    - Memperbaiki filter animasi untuk web
April 11, 2023
    - Mempelajari panduan laporan
April 12, 2023
    - Mempelajari panduan laporan PKL
April 13, 2023
    - Proses mengerjakan laporan PKL
April 14, 2023
    - Init laravel
    - Design database exit survey
April 15, 2023
    - Belajar livewire
    - Belajar Docker
April 17, 2023
    - Merekap referensi laporan PKL
April 18, 2023
    - Menyelesaikan bab kata pengantar
    - Manage github profile GDSC
    - Proses bab pendahuluan
Mei 3, 2023
    - Mengorganisir nodejs, yarn, npx dengan n version manager
    - Fixing path node dan yarn
Mei 4, 2023
    - Belajar deploy react native dengan eas-cli
Mei 5, 2023
    - Auto build expo dengan git action
Mei 6, 2023
    - Membuat flow git action untuk unit testing laravel
Mei 8, 2023
    - Belajar tailwind & daisyui kit
Mei 9, 2023
    - Membuat dynamic theme dengan daisyui
    - Belajar laravel component
Mei 10, 2023
    - Design site exit survey
May 11, 2023
    - Redesign structure database exit survey
Mei 12, 2023
    - Design certificate
    - Send certificate dan token ke semua participant
Mei 13, 2023
    - Design social media post untuk next event
    - Schedule dan send event
May 15, 2023
    - Mengumpulkan material untuk laporan PKL
    - Update table database exit survey
Mei 16, 2023
    - Belajar laravel component
    - Membuat factory dan seeder untuk testing
Mei 17, 2023
    - Belajar animasi tailwind floating label di form
    - Menyusun text field sebagai component
May 19, 2023
- Belajar tailwind responsive design
- Menambahkan progress bar di layout form
May 20, 2023
- Mengerjakan Automating Infrastructure on Google Cloud with Terraform part 1-3
May 22, 2023
- Mengerjakan Automating Infrastructure on Google Cloud with Terraform part 4-5
- Belajar docker
May 23, 2023
    - Migrating website to Microservices on Google Kubernetes Engine
    - Recap post event GDSC
    - Menyelesaikan materi GDSC untuk next event
May 24, 2023
Menyelesaikan quest Build a Website on Google Cloud dan Implement DevOps in Google Cloud
May 25, 2023
Menyelesaikan Create and Manage Cloud Resources
May 26, 2023
Menyelesaikan Deploy to Kubernetes in Google Cloud
May 27, 2023
    - Menyelesaikan Ensure Access & Identity in Google Cloud
    - Membuat soal untuk GDSC
May 30, 2023
    - Set Up and Configure a Cloud Environment in Google Cloud
    - BigQuery Basics for Data Analysts
May 31, 2023
    - Create ML Models with BigQuery ML
    - Deploy and Manage Cloud Environments with
Juni 2, 2023
Serverless Firebase Development
Juni 5, 2023
    - Rekap absensi GDSC
    - Belajar expo stack navigation
June 6, 2023
    - Belajar themes ui kitten
    - Belajar init ui kitten
June 7, 2023
    - Firebase auth
    - Submiting gdsc exit
June 8, 2023
    - Membuat design project feature dan struktur database
June 9, 2023
    - Meet project brix
June 10, 2023
    - Membuat dokumentasi pekiraan jam per feature
June 12, 2023
    - Latihan expo dan package basic react native
    - Belajar zettelkastein obsidian
June 13, 2023
    - Melanjutkan kembali tailwind laravel untuk exit survey
    - Menerapkan zettelkastein partial
June 14, 2023
    - Belajar eslint typescript react
June 15, 2023
    - Apply eslint project expo
    - Integrasi eslint dengan webstorm
June 17, 2023
    - Design Intro slide screen
    - Toggle theme using theme wrapper provider
June 19, 2023
    - Design Auth Screen
June 20, 2023
    - Type checks navigation using typescript
    - Auth navigation
June 21, 2023
    - Bottom Navigation
    - Home Design
June 22, 2023
    - Replace flatlist dgn section list
    - Mereview LPK Tahunan
June 23, 2023
    - Proses design structure component exit survey partial
June 24, 2023
    - Proses laporan pkl Bab IV


