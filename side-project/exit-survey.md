Hi there!

We just wanted to send a quick reminder that our form will be closing soon. If you haven't had a chance to fill it out yet, please do so as soon as possible! We value your input and it will really help us in planning future meetings.

Thanks so much for your time and have a great day!
Halo!

Kami hanya ingin mengirimkan pengingat cepat bahwa survei kami tentang ketersediaan dan spesifikasi laptop akan segera ditutup. Jika kamu belum sempat mengisinya, tolong segera diisi secepatnya ya! Kami sangat menghargai masukanmu dan ini akan sangat membantu kami dalam merencanakan pertemuan di masa depan.

Hi there! Kita cuma mau ngingetin kalo form nya akan ditutup besok. Jadi kalau ada yang masih belum sempat mengisi, segera diisi ya! 
Terima kasih banyak dan semoga harimu menyenangkan!


