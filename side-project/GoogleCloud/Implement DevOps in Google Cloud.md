#### Preparation
https://www.youtube.com/watch?v=LjhWmJrVP9o
``` shell
gcloud config set project VALUE

export PROJECT_ID=$(gcloud config get-value project)
export PROJECT_NUMBER=$(gcloud projects describe $PROJECT_ID --format='value(projectNumber)')
export REGION=us-central1
export CLUSTER_NAME=hello-cluster
export ZONE=us-central1-a
export REPO=**my-repository**

gcloud config set compute/region $REGION
```


#### Task 1: Create the lab resources
1. Enable the APIs for GKE, Cloud Build, Cloud Source Repositories
```shell
gcloud services enable container.googleapis.com \
    cloudbuild.googleapis.com \
    sourcerepo.googleapis.com
```
2.  Add the Kubernetes Developer role for the Cloud Build service account:
```shell
gcloud projects add-iam-policy-binding $PROJECT_ID \
--member=serviceAccount:$(gcloud projects describe $PROJECT_ID \
--format="value(projectNumber)")@cloudbuild.gserviceaccount.com --role="roles/container.developer"
```
3. Configure git -> replace email with lab email, and name is ur name
```shell
git config --global user.email <email> 
git config --global user.name <name>
```
4. Craete an Artifact Registry Docker repository named **my-repository** in the `us-central1` region to store your container images
```shell
gcloud artifacts repositories create my-repository \
  --repository-format=docker \
  --location=$REGION \
  --description="Subscribe to ME"
```
5. Create a GKE cluster named `hello-cluster` with the following configuration:

| Setting  | Value |
| ----- | -------------- |
| zone | us-central1-a |
| **Release channel** | **Regular** |
| **Cluster version** | `1.25.5-gke.2000` _or newer_ |
| **Cluster autoscaler** | **Enabled** |
| **Number of nodes** | 3 |
| **Minimum nodes** | 2 |
| **Maximum nodes** | 6 |

``` shell

gcloud container --project "$PROJECT_ID" \ 
	clusters create "$CLUSTER_NAME" --zone "$ZONE" \
	--cluster-version "1.25.8-gke.500" \
	--release-channel "regular" \
	--enable-autoscaling --num-nodes "3" --min-nodes "2" \
	--max-nodes "6"
```
Optional version
``` shell
gcloud container --project "$PROJECT_ID" \ 
	clusters create "$CLUSTER_NAME" --zone "$ZONE" \
	--no-enable-basic-auth --cluster-version "1.25.8-gke.500" \
	--release-channel "regular" --machine-type "e2-medium" \
	--image-type "COS_CONTAINERD" --disk-type "pd-balanced" --disk-size "100" \
	--metadata disable-legacy-endpoints=true --logging=SYSTEM,WORKLOAD \
	--monitoring=SYSTEM --enable-ip-alias --network \
	"projects/$PROJECT_ID/global/networks/default" --subnetwork \
	"projects/$PROJECT_ID/regions/$REGION/subnetworks/default" \
	--no-enable-intra-node-visibility --default-max-pods-per-node "110" \
	--enable-autoscaling --min-nodes "2" --max-nodes "6" \
	--location-policy "BALANCED" --no-enable-master-authorized-networks --addons \
	HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable \
	autoupgrade --enable-autorepair --max-surge-upgrade 1 \
	--max-unavailable-upgrade 0 --enable-shielded-nodes --node-locations "$ZONE"
```
6.  Create the `prod` and `dev` namespaces on your cluster.
``` shell
kubectl create namespace prod
kubectl create namespace dev
```

#### Task 2: Create a repository in Cloud Source Repositories
1.  Create an empty repository named **sample-app** in Cloud Source Repositories.
```sh
gcloud source repos create simple-app
```
2.  Clone the **sample-app** Cloud Source Repository in Cloud Shell.
```sh
git clone https://source.developers.google.com/p/$PROJECT_ID/r/sample-app
```
3.  Use the following command to copy the sample code into your `sample-app` directory
``` sh
cd ~
gsutil cp -r gs://spls/gsp330/sample-app/* sample-app
```
4.  Make your first commit with the sample code added to your `sample-app` directory, and push the changes to the **master** branch.
```sh
cd ~/sample-app/
git init
git add .
git commit -m ":tada:"
git push -u origin master
```
6.  Create a branch named **dev**. Make a commit with the sample code added to your `sample-app` directory and push the changes to the **dev** branch.
```sh
git checkout -b dev
git push -u origin dev
```
7. Verify you have the sample code and branches stored in the Source Repository.
go to **Artifact Registry > Repositories** to verify

#### Task 3. Create the Cloud Build Triggers
![example](https://cdn.qwiklabs.com/joBzWe0xa5NJ1uE63FgrF2KVJCswQcPKxddT8O4lJS0%3D)

1.  Create a Cloud Build Trigger named **sample-app-prod-deploy** that with the following configurations:
    -   Event: **Push to a branch**
    -   Source Repository: `sample-app`
    -   Branch: `^master$`
    -   Cloud Build Configuration File: `cloudbuild.yaml`
2.  Create a Cloud Build Trigger named **sample-app-dev-deploy** that with the following configurations:
    -   Event: **Push to a branch**
    -   Source Repository: `sample-app`
    -   Branch: `^dev$`
    -   Cloud Build Configuration File: `cloudbuild-dev.yaml`

#### Task 4. Deploy the first versions of the application
### Build the first development deployment
```sh
export REPO=my-repository
COMMIT_ID="$(git rev-parse --short=7 HEAD)"

gcloud builds submit --tag="${REGION}-docker.pkg.dev/${PROJECT_ID}/$REPO/hello-cloudbuild:${COMMIT_ID}" .

```
>**Get the image link for editing dev/deployment TODO**
```sh
git checkout dev
```
1.  In Cloud Shell, inspect the `cloudbuild-dev.yaml` file to see the steps in the build process. Fill in the `<version>` on lines 10 and 15 with `v1.0`.
2.  Navigate to the `dev/deployment.yaml` file and fill in the `<todo>` on line 17 with the correct container image name.
>**Hint:** there are two containers that are built, one for the production app and one for the development app. Make sure to reference the correct one!

3.  Make a commit with your changes on the `dev` branch and push changes to trigger the **sample-app-dev-deploy** build job.
```sh
git add .
git commit -m ":pencil:"
git push -u origin dev
```
4.  Verify your build executed successfully, and verify the **development-deployment** application was deployed onto the `dev` namespace of the cluster.
5.  [Expose](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps#using_kubectl_expose_to_create_a_service) the **development-deployment** deployment to a LoadBalancer service named `dev-deployment-service` on port 8080, and set the target port of the container to the one specified in the Dockerfile.
6.  Navigate to the Load Balancer IP of the service and add the `/blue` entry point at the end of the URL to verify the application is up and running. It should resemble something like the following: `http://34.135.97.199:8080/blue`.

##### Build the first production deployment
```sh
git checkout master
```
1.  Inspect the `cloudbuild.yaml` file to see the steps in the build process. Fill in the `<version>` on lines **10** and **15** with `v1.0`.
2.  Navigate to the `prod/deployment.yaml` file and fill in the `<todo>` on line 17 with the correct container image name.
>**Note:** your container image name cannot contain any variables, so you will need to use the _full path_ of your image in Artifact Registry.

3.  Make a commit with your changes on the `master` branch and push changes to trigger the **sample-app-prod-deploy** build job.
```sh
git add .
git commit -m ":pencil: Init prod"
git push -u origin master
```
4.  Verify your build executed successfully, and verify the **production-deployment** application was deployed onto the `prod` namespace of the cluster.
5.  [Expose](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps#using_kubectl_expose_to_create_a_service) the **production-deployment** deployment on the `prod` namespace to a LoadBalancer service named `prod-deployment-service` on port 8080, and set the target port of the container to the one specified in the Dockerfile.
6.  Navigate to the Load Balancer IP of the service and add the `/blue` entry point at the end of the URL to verify the application is up and running. It should resemble something like the following: `http://34.135.245.19:8080/blue`.

#### Task 5. Deploy the second versions of the application
##### Build the second development deployment
```sh
git checkout dev
```
2. In the `main.go` file, update the `main()` function to the following:
```go
func main() {
	http.HandleFunc("/blue", blueHandler)
	http.HandleFunc("/red", redHandler)
	http.ListenAndServe(":8080", nil)
}
```
3. Add the following function inside of the `main.go` file:
```go
func redHandler(w http.ResponseWriter, r *http.Request) {
	img := image.NewRGBA(image.Rect(0, 0, 100, 100))
	draw.Draw(img, img.Bounds(), &image.Uniform{color.RGBA{255, 0, 0, 255}}, image.ZP, draw.Src)
	w.Header().Set("Content-Type", "image/png")
	png.Encode(w, img)
}
```
4. Inspect the `cloudbuild-dev.yaml` file to see the steps in the build process. Update the version of the Docker image to `v2.0`.
5. Navigate to the `dev/deployment.yaml` file and update the container image name to the new version (`v2.0`).
###### from this
`us-central1-docker.pkg.dev/qwiklabs-gcp-03-9402d262f697/my-repository/hello-cloudbuild:fa26057`
##### to this
`us-central1-docker.pkg.dev/qwiklabs-gcp-03-9402d262f697/my-repository/hello-cloudbuild:v1.0`
6. Make a commit with your changes on the `dev` branch and push changes to trigger the **sample-app-dev-deploy** build job.

##### Build the second production deployment
```sh
git checkout master
```
2. In the `main.go` file, update the `main()` function to the following:
```go
func main() {
	http.HandleFunc("/blue", blueHandler)
	http.HandleFunc("/red", redHandler)
	http.ListenAndServe(":8080", nil)
}
```
3. Add the following function inside of the `main.go` file:
```go
func redHandler(w http.ResponseWriter, r *http.Request) {
	img := image.NewRGBA(image.Rect(0, 0, 100, 100))
	draw.Draw(img, img.Bounds(), &image.Uniform{color.RGBA{255, 0, 0, 255}}, image.ZP, draw.Src)
	w.Header().Set("Content-Type", "image/png")
	png.Encode(w, img)
}
```
4. Inspect the `cloudbuild.yaml` file to see the steps in the build process. Update the version of the Docker image to `v2.0`.
5. Navigate to the `prod/deployment.yaml` file and update the container image name to the new version (`v2.0`).
###### from this
`us-central1-docker.pkg.dev/qwiklabs-gcp-03-9402d262f697/my-repository/hello-cloudbuild:fa26057`
##### to this
`us-central1-docker.pkg.dev/qwiklabs-gcp-03-9402d262f697/my-repository/hello-cloudbuild:v1.0`
6. Make a commit with your changes on the `master` branch and push changes to trigger the **sample-app-prod-deploy** build job.

#### Task 6. Roll back the production deployment
1. Roll back the **production-deployment** to use the `v1.0` version of the application
1. In the Cloud console, go to **Cloud Build > Dashboard**.
2. Click on _View all_ link under **Build History** for the **hello-cloudbuild-env** repository.
3. Click on the second most recent build available.