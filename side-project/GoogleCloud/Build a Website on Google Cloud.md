# Challenge Lab
```shell
gcloud config set project VALUE

export MONOLITH_IDENTIFIER=
export CLUSTER_NAME=
export ORDERS_IDENTIFIER=
export PRODUCTS_IDENTIFIER=
export FRONTEND_IDENTIFIER=

gcloud services enable cloudbuild.googleapis.com
gcloud services enable container.googleapis.com

```

#### Task 1: Download the monolith code and build your container
```shell
git clone https://github.com/googlecodelabs/monolith-to-microservices.git
cd ~/monolith-to-microservices
./setup.sh
nvm install --lts
cd ~/monolith-to-microservices/monolith
gcloud builds submit --tag gcr.io/${GOOGLE_CLOUD_PROJECT}/${MONOLITH_IDENTIFIER}:1.0.0 .
```

#### Task 2: Create a kubernetes cluster and deploy the application
```shell
gcloud config set compute/zone us-central1-a
gcloud container clusters create $CLUSTER_NAME --num-nodes 3
gcloud container clusters get-credentials $CLUSTER_NAME

kubectl create deployment $MONOLITH_IDENTIFIER --image=gcr.io/${GOOGLE_CLOUD_PROJECT}/${MONOLITH_IDENTIFIER}:1.0.0

kubectl expose deployment $MONOLITH_IDENTIFIER --type=LoadBalancer --port 80 --target-port 8080
```
`kubectl get all`

#### Task 3: Create a containerized version of your Microservices
```shell
cd ~/monolith-to-microservices/microservices/src/orders
gcloud builds submit --tag gcr.io/${GOOGLE_CLOUD_PROJECT}/${ORDERS_IDENTIFIER}:1.0.0 .

cd ~/monolith-to-microservices/microservices/src/products
gcloud builds submit --tag gcr.io/${GOOGLE_CLOUD_PROJECT}/${PRODUCTS_IDENTIFIER}:1.0.0 .
```

#### Task 4: Deploy the new microservices
```shell
kubectl create deployment ${ORDERS_IDENTIFIER} --image=gcr.io/${GOOGLE_CLOUD_PROJECT}/${ORDERS_IDENTIFIER}:1.0.0  

kubectl expose deployment ${ORDERS_IDENTIFIER} --type=LoadBalancer --port 80 --target-port 8081

kubectl create deployment ${PRODUCTS_IDENTIFIER} --image=gcr.io/${GOOGLE_CLOUD_PROJECT}/${PRODUCTS_IDENTIFIER}:1.0.0

kubectl expose deployment ${PRODUCTS_IDENTIFIER} --type=LoadBalancer --port 80 --target-port 8082
```
`kubectl get service`
`kubectl delete service {orders | products}`
`kubectl delete deployment {orders | products}`

#### Task 5: Configure the Frontend microservice
```shell
cd ~/monolith-to-microservices/react-app
nano .env
```

#### Task 6: Create a containerized version of the Frontend microservice
```shell
cd ~/monolith-to-microservices/microservices/src/frontend

gcloud builds submit --tag gcr.io/${GOOGLE_CLOUD_PROJECT}/${FRONTEND_IDENTIFIER}:1.0.0 .
```

#### Task 7: Deploy the Frontend microservice
```shell
kubectl create deployment ${FRONTEND_IDENTIFIER} --image=gcr.io/${GOOGLE_CLOUD_PROJECT}/${FRONTEND_IDENTIFIER}:1.0.0

kubectl expose deployment ${FRONTEND_IDENTIFIER} --type=LoadBalancer --port 80 --target-port 8080
```