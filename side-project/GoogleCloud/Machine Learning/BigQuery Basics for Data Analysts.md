#### Accessing public datasets
1. Click on **+ ADD**.
2. Choose **Star a project by name**.
3. Enter project name as **bigquery-public-data**.
4. Click **STAR**.
### BigQuery Command Line
```sh
bq show bigquery-public-data:samples.shakespeare
```
- `bq` to invoke the BigQuery command line tool
- `show` is the action
- Then you're listing the name of the `project:public dataset.table` in BigQuery that you want to see.
#### Run a query
``` sh
bq query --use_legacy_sql=false \
'SELECT
   word,
   SUM(word_count) AS count
 FROM
   `bigquery-public-data`.samples.shakespeare
 WHERE
   word LIKE "%raisin%"
 GROUP BY
   word'
```
##### List existing dataset
```sh
bq ls
```
###### Specify the dataset
```sh
bq ls bigquery-public-data:
```
##### New dataset named babynames
```sh
bq mk babynames
```
##### Upload dataset
- Download
```sh
curl -LO http://www.ssa.gov/OACT/babynames/names.zip
```
- Create table from .txt (csv)
```sh
bq load babynames.names2010 yob2010.txt name:string,gender:string,count:integer
```
##### List table
```sh
bq ls babynames
```
##### Show schema
```sh
bq show babynames.names2010
```
##### Extra
```sh
bq query "SELECT name,count FROM babynames.names2010 WHERE gender = 'M' ORDER BY count ASC LIMIT 5"
```
##### Remove dataset
```sh
bq rm -r babynames
```
