```sh
export Docker_Image_and_Tag_Name=valkyrie-app:v0.0.3
export REPOSITORY=valkyrie-docker-repo
export PROJECT_ID=qwiklabs-gcp-00-cc6f868a5634
```
### Task 1. Create a Docker image and store the Dockerfile
1. This will install the marking scripts you will use to help check your progress.
```sh
source <(gsutil cat gs://cloud-training/gsp318/marking/setup_marking_v2.sh)
```
2. Use Cloud Shell to clone the `valkyrie-app` source code repository into the `~/marking` directory. You can use the following command:
```sh
gcloud source repos clone valkyrie-app
```
3. Create `valkyrie-app/Dockerfile` and add the configuration below:
```sh
FROM golang:1.10
WORKDIR /go/src/app
COPY source .
RUN go install -v
ENTRYPOINT ["app","-single=true","-port=8080"]
```
```sh
cd valkyrie-app
cat > Dockerfile <<EOF
FROM golang:1.10
WORKDIR /go/src/app
COPY source .
RUN go install -v
ENTRYPOINT ["app","-single=true","-port=8080"]
EOF
```
4. Use `valkyrie-app/Dockerfile` to create a Docker image called **`valkyrie-app`** with the tag **`v0.0.3`**.
```sh
docker build -t $Docker_Image_and_Tag_Name .
```
5. Once you have created the Docker image, and before clicking **Check my progress**, run the following command to perform the local check of your work:
```sh
cd ..
cd marking
./step1_v2.sh
```
-----------------------------------------------------------------------------------------------------------------------------------------------------------------

### Task 2. Test the created Docker image
1. Launch a container using the image **`valkyrie-app:v0.0.3`**.
- You need to map the host’s port 8080 to port 8080 on the container.
- Add `&` to the end of the command to cause the container to run in the background.
When your container is running you will see the page by **Web Preview**.
```sh
cd ~/marking/valkyrie-app
docker run -p 8080:8080 $Docker_Image_and_Tag_Name &
```
2. Once you have your container running, and before clicking **Check my progress**, run the following command to perform the local check of your work.
```sh
cd ~/marking
./step2_v2.sh
bash ~/marking/step2_v2.sh
```


-----------------------------------------------------------------------------------------------------------------------------------------------------------------

### Task 3. Push the Docker image to the Artifact Registry
1. Create a repository named `valkyrie-docker-repo` in Artifact Registry. Use **Docker** as the format and use the `us-central1` region as the location.
```sh
cd ~/marking/valkyrie-app

gcloud artifacts repositories create $REPOSITORY \
    --repository-format=docker \
    --location=us-central1 \
    --description="subcribe to ME" \
    --async 
```
2. Before you can push or pull images, configure Docker to use the Google Cloud CLI to authenticate requests to Artifact Registry. You will need to set up authentication to Docker repositories in the `us-central1` region.
```sh
gcloud auth configure-docker us-central1-docker.pkg.dev
```
3. [Re-tag](https://cloud.google.com/artifact-registry/docs/docker/pushing-and-pulling#tag) the container to be able push it to the repository. The format should resemble the following: `LOCATION-docker.pkg.dev/PROJECT-ID/REPOSITORY/IMAGE`.
```sh
docker images
docker tag <Image_ID> us-central1-docker.pkg.dev/$PROJECT_ID/$REPOSITORY/$Docker_Image_and_Tag_Name
```
4. Push the Docker image to the Artifact Registry.
```sh
docker push us-central1-docker.pkg.dev/$PROJECT_ID/$REPOSITORY/$Docker_Image_and_Tag_Name
```

-----------------------------------------------------------------------------------------------------------------------------------------------------------------



### Task 4. Create and expose a deployment in Kubernetes
Kurt created the `deployment.yaml` and `service.yaml` to deploy your new container image to a Kubernetes cluster (called valkyrie-dev). The two files are in `valkyrie-app/k8s`.

1. Get the Kubernetes credentials before you deploy the image onto the Kubernetes cluster.
```sh
gcloud container clusters get-credentials valkyrie-dev --zone us-east1-d
```
2. Before you create the deployments, make sure you check the `deployment.yaml` file. Kurt thinks they need some values set (he thinks he left some placeholder values).
```sh
sed -i s#IMAGE_HERE#us-central1-docker.pkg.dev/$PROJECT_ID/$REPOSITORY/$Docker_Image_and_Tag_Name#g k8s/deployment.yaml
```
3. Create the deployments from the `deployment.yaml` and `service.yaml` files.
```sh

kubectl create -f k8s/deployment.yaml
kubectl create -f k8s/service.yaml
```
4. From the Navigation Menu, select **Kubernetes Engine** > **Services & Ingress**. Click on the load balancer IP Address of the `valkyrie-dev` service to verify your services are up and running.






