https://astrodevil.medium.com/ensure-access-identity-in-google-cloud-challenge-lab-9920d0707a70
https://www.youtube.com/watch?v=9Wc7e1cIdsc

## Task 1. Create a custom security role.
Your first task is to create a new custom IAM security role called `Custom Securiy Role` that will provide the Google Cloud storage bucket and object permissions required to be able to create and update storage objects.
```sh
gcloud config set compute/zone us-central1-a
export CUSTOM_ROLE=orca_storage_creator_457
export SERVICE_ACCOUNT=orca-private-cluster-205-sa
export CLUSTER_NAME=orca-cluster-158
```
```sh
nano role-definition.yaml
```
```yaml
title: "orca_storage_creator_457"
description: "Permissions"
stage: "ALPHA"
includedPermissions:
- storage.buckets.get
- storage.objects.get
- storage.objects.list
- storage.objects.update
- storage.objects.create
```

```sh
gcloud iam service-accounts create $SERVICE_ACCOUNT --display-name "Orca Private Cluster Service Account"

gcloud iam roles create $CUSTOM_ROLE --project $DEVSHELL_PROJECT_ID --file role-definition.yaml
```

## Task 2. Create a service account.
Your second task is to create the dedicated service account that will be used as the service account for your new private cluster. You must name this account `Service Account`.
```sh
gcloud iam service-accounts create $SERVICE_ACCOUNT --display-name "Orca Private Cluster Service Account"
```

## Task 3. Bind a custom security role to a service account.
You must now bind the Cloud Operations logging and monitoring roles that are required for Kubernetes Engine Cluster service accounts as well as the custom IAM role you created for storage permissions to the Service Account you created earlier.
```sh

gcloud projects add-iam-policy-binding $DEVSHELL_PROJECT_ID --member serviceAccount:$SERVICE_ACCOUNT@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --role roles/monitoring.viewer

  

gcloud projects add-iam-policy-binding $DEVSHELL_PROJECT_ID --member serviceAccount:$SERVICE_ACCOUNT@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --role roles/monitoring.metricWriter

  

gcloud projects add-iam-policy-binding $DEVSHELL_PROJECT_ID --member serviceAccount:$SERVICE_ACCOUNT@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --role roles/logging.logWriter

  

gcloud projects add-iam-policy-binding $DEVSHELL_PROJECT_ID --member serviceAccount:$SERVICE_ACCOUNT@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --role projects/$DEVSHELL_PROJECT_ID/roles/$CUSTOM_ROLE

  
```

## Task 4. Create and configure a new Kubernetes Engine private cluster
You must now use the service account you have configured when creating a new Kubernetes Engine private cluster. The new cluster configuration must include the following:

- The cluster must be called `orca-cluster-158`
- The cluster must be deployed to the subnet `orca-build-subnet`
- The cluster must be configured to use the `orca-private-cluster-205-sa` service account.
- The private cluster options `enable-master-authorized-networks`, `enable-ip-alias`, `enable-private-nodes`, and `enable-private-endpoint` must be enabled.

Once the cluster is configured you must add the internal ip-address of the `orca-jumphost` compute instance to the master authorized network list.
```sh
gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --master-ipv4-cidr=172.16.0.64/28 --network orca-build-vpc --subnetwork orca-build-subnet --enable-master-authorized-networks --master-authorized-networks 192.168.10.2/32 --enable-ip-alias --enable-private-nodes --enable-private-endpoint --service-account $SERVICE_ACCOUNT@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com --zone us-east1-b
```

## Task 5. Deploy an application to a private Kubernetes Engine cluster.

```sh
gcloud config set compute/zone us-east1-b

  

gcloud container clusters get-credentials $CLUSTER_NAME --internal-ip  
  
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin

  

kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0

  

kubectl expose deployment hello-server --name orca-hello-service --type LoadBalancer --port 80 --target-port 8080

```


```
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin
echo "export USE_GKE_GCLOUD_AUTH_PLUGIN=True" >> ~/.bashrc
source ~/.bashrc
gcloud container clusters get-credentials orca-cluster-158 --internal-ip --project=qwiklabs-gcp-00-12c2c7afc261 --zone us-east1-b
```

```
gcloud container clusters get-credentials orca-cluster-158 --internal-ip --zone us-east1-b --project qwiklabs-gcp-00-12c2c7afc261

kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0
```