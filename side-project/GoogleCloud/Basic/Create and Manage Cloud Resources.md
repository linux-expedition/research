# Getting Started with Cloud Shell and gcloud
##### List the compute instance available in the project:
```sh
gcloud compute instances list
```
##### List the firewall rules in the project
```sh
gcloud compute firewall-rules list
```
#### Connecting to your VM instance
To connect to your VM with SSH, run the following command
```sh
gcloud compute ssh gcelab2 --zone $ZONE
```
#### Updating the firewall
##### 1. Add a tag to the virtual machine:
```sh
gcloud compute instances add-tags gcelab2 --tags http-server,https-server
```
##### 2. Update the firewall rule to allow:
```sh
gcloud compute firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server
```
##### 3. List the firewall rules for the project:
```sh
gcloud compute firewall-rules list --filter=ALLOW:'80'
```
##### 4. Verify communication is possible for http to the virtual machine:
```sh
curl http://$(gcloud compute instances list --filter=name:gcelab2 --format='value(EXTERNAL_IP)')
```
# GKE CLuster
##### Create GKE Cluster
```sh
gcloud container clusters create --machine-type=e2-medium --zone=assigned_at_lab_start lab-cluster 
```
##### Get authentication credentials for the cluster
**Authenticate with the cluster**:
```sh
gcloud container clusters get-credentials lab-cluster 
```
#### Deploy an application to the cluster
##### **create a new Deployment**
```sh
kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0
```
##### **create a Kubernetes Service**
```sh
kubectl expose deployment hello-server --type=LoadBalancer --port 8080
```

# Set Up Network and HTTP Load Balancers
#### Set the default region and zone for all resources
```sh
gcloud config set compute/zone us-west1-a
gcloud config set compute/region us-west1
```
#### Create multiple web server instances
##### Create a virtual machine www1 in your default zone
```sh
  gcloud compute instances create www1 \
    --zone=us-west1-a \
    --tags=network-lb-tag \
    --machine-type=e2-small \
    --image-family=debian-11 \
    --image-project=debian-cloud \
    --metadata=startup-script='#!/bin/bash
      apt-get update
      apt-get install apache2 -y
      service apache2 restart
      echo "
<h3>Web Server: www1</h3>" | tee /var/www/html/index.html'
```
##### Create a virtual machine www2 in your default zone.
```sh
  gcloud compute instances create www2 \
    --zone=us-west1-a \
    --tags=network-lb-tag \
    --machine-type=e2-small \
    --image-family=debian-11 \
    --image-project=debian-cloud \
    --metadata=startup-script='#!/bin/bash
      apt-get update
      apt-get install apache2 -y
      service apache2 restart
      echo "
<h3>Web Server: www2</h3>" | tee /var/www/html/index.html'
```
##### Create a virtual machine www3 in your default zone.
```sh
  gcloud compute instances create www3 \
    --zone=us-west1-a \
    --tags=network-lb-tag \
    --machine-type=e2-small \
    --image-family=debian-11 \
    --image-project=debian-cloud \
    --metadata=startup-script='#!/bin/bash
      apt-get update
      apt-get install apache2 -y
      service apache2 restart
      echo "
<h3>Web Server: www3</h3>" | tee /var/www/html/index.html'
```
##### Create a firewall rule to allow external traffic to the VM instances:
```sh
gcloud compute firewall-rules create www-firewall-network-lb \
    --target-tags network-lb-tag --allow tcp:80
```
##### Verify that each instance is running with `curl`, replacing **IP_ADDRESS** with the IP address for each of your VMs
```sh
gcloud compute instances list
curl http://[IP_ADDRESS]
```


```sh
   gcloud compute addresses create network-lb-ip-1 \
    --region us-west1 
```
```sh
gcloud compute http-health-checks create basic-check
```
```sh
  gcloud compute target-pools create www-pool \
    --region us-west1 --http-health-check basic-check
```
```sh
gcloud compute target-pools add-instances www-pool \
    --instances www1,www2,www3
```
```sh
gcloud compute forwarding-rules create www-rule \
    --region  us-west1 \
    --ports 80 \
    --address network-lb-ip-1 \
    --target-pool www-pool
```


```sh
gcloud compute forwarding-rules describe www-rule --region us-west1
```
```sh
IPADDRESS=$(gcloud compute forwarding-rules describe www-rule --region us-west1 --format="json" | jq -r .IPAddress)
```


```sh
gcloud compute instance-templates create lb-backend-template \
   --region=us-west1 \
   --network=default \
   --subnet=default \
   --tags=allow-health-check \
   --machine-type=e2-medium \
   --image-family=debian-11 \
   --image-project=debian-cloud \
   --metadata=startup-script='#!/bin/bash
     apt-get update
     apt-get install apache2 -y
     a2ensite default-ssl
     a2enmod ssl
     vm_hostname="$(curl -H "Metadata-Flavor:Google" \
     http://169.254.169.254/computeMetadata/v1/instance/name)"
     echo "Page served from: $vm_hostname" | \
     tee /var/www/html/index.html
     systemctl restart apache2'
```
```sh
gcloud compute instance-groups managed create lb-backend-group \
   --template=lb-backend-template --size=2 --zone=us-west1-a 
```
```sh
gcloud compute firewall-rules create fw-allow-health-check \
  --network=default \
  --action=allow \
  --direction=ingress \
  --source-ranges=130.211.0.0/22,35.191.0.0/16 \
  --target-tags=allow-health-check \
  --rules=tcp:80
```


# Challenge
## Create a project jumphost instance
- Name the instance **`nucleus-jumphost-798`**.
- Use an _f1-micro_ machine type.
- Use the default image type (Debian Linux).
```sh
gcloud compute instances create nucleus-jumphost-798 \
  --network nucleus-vpc \
  --zone us-central1-b  \
  --machine-type f1-micro  \
  --image-family debian-11  \
  --image-project debian-cloud 
```
## Create a Kubernetes service cluster
- Create a zonal cluster using `us-central1-b`.
- Use the Docker container hello-app (`gcr.io/google-samples/hello-app:2.0`) as a placeholder; the team will replace the container with their own work later.
- Expose the app on port `8080`.
```sh
gcloud container clusters create nucleus-backend \
          --num-nodes 1 \
          --network nucleus-vpc \
          --zone us-central1-b


gcloud container clusters get-credentials nucleus-backend \
          --zone us-central1-b

kubectl create deployment hello-server \
          --image=gcr.io/google-samples/hello-app:2.0


kubectl expose deployment hello-server \
          --type=LoadBalancer \
          --port 8080
```
## Set up an HTTP load balancer
```sh
cat << EOF > startup.sh
#! /bin/bash
apt-get update
apt-get install -y nginx
service nginx start
sed -i -- 's/nginx/Google Cloud Platform - '"\$HOSTNAME"'/' /var/www/html/index.nginx-debian.html
EOF
```
**You need to:**
- Create an instance template.
- Create a target pool.
- Create a managed instance group.
- Create a firewall rule named as `permit-tcp-rule-200` to allow traffic (80/tcp).
- Create a health check.
- Create a backend service, and attach the managed instance group with named port (http:80).
- Create a URL map, and target the HTTP proxy to route requests to your URL map.
- Create a forwarding rule.
```sh
gcloud compute instance-templates create web-server-template \
          --metadata-from-file startup-script=startup.sh \
          --network nucleus-vpc \
          --machine-type g1-small \
          --region us-central1
```
**Creating a managed instance group:**
```sh
gcloud compute instance-groups managed create web-server-group \
          --base-instance-name web-server \
          --size 2 \
          --template web-server-template \
          --region us-central1
```
**Creating a firewall rule to allow traffic (80/tcp)**
```sh
gcloud compute firewall-rules create permit-tcp-rule-200 \
          --allow tcp:80 \
          --network nucleus-vpc
```
**Creating a health check:**
```sh
gcloud compute http-health-checks create http-basic-check
gcloud compute instance-groups managed \
          set-named-ports web-server-group \
          --named-ports http:80 \
          --region us-central1
```
**Creating a backend service and attach the managed instance group**
```sh
gcloud compute backend-services create web-server-backend \
          --protocol HTTP \
          --http-health-checks http-basic-check \
          --global
gcloud compute backend-services add-backend web-server-backend \
          --instance-group web-server-group \
          --instance-group-region us-central1 \
          --global
```
**Creating a URL map and target HTTP proxy to route requests to your URL map**
```sh
gcloud compute url-maps create web-server-map \
          --default-service web-server-backend

gcloud compute target-http-proxies create http-lb-proxy \
          --url-map web-server-map
```
**Creating forwarding rule**
```sh
gcloud compute forwarding-rules create http-content-rule \
        --global \
        --target-http-proxy http-lb-proxy \
        --ports 80
gcloud compute forwarding-rules list
```
