Prerequisites
[[Create and Manage Cloud Resources]]
[[Deploy to Kubernetes in Google Cloud]]
[[Ensure Access & Identity in Google Cloud]]

## Task 1. Create development VPC manually

- Create a VPC called `griffin-dev-vpc` with the following subnets only:
    
    - `griffin-dev-wp`
        - IP address block: `192.168.16.0/20`
    - `griffin-dev-mgmt`
        - IP address block: `192.168.32.0/20`

1. In the Cloud Console, navigate to **Navigation menu** (![Navigation menu icon](https://cdn.qwiklabs.com/tkgw1TDgj4Q%2BYKQUW4jUFd0O5OEKlUMBRYbhlCrF0WY%3D)) > **VPC network** > **VPC networks**.
2. Click on the Create VPC network.
3. In the Name, field write `griffin-dev-vpc`.
4. Select Custom for the Subnet creation mode.
	Name: ...wp || mgmt
	Region:- `us-east1`
	Range: ...
	and so on

## Task 2. Create production VPC manually

- Create a VPC called `griffin-prod-vpc` with the following subnets only:
    
    - `griffin-prod-wp`
        - IP address block: `192.168.48.0/20`
    - `griffin-prod-mgmt`
        - IP address block: `192.168.64.0/20`

1. Activate the cloud shell and type the following command.
```sh
gsutil cp -r gs://cloud-training/gsp321/dm ~/
```
2. edit `prod-network.yaml` configuration file. Replace SET_REGION to `us-east1-b` zone in the editor, and save the changes.
```sh
cd dm
sed -i s/SET_REGION/us-east1/g prod-network.yaml
```
3. create the production VPC network with the configuration files
```sh
gcloud deployment-manager deployments create griffin-prod --config prod-network.yaml
```

## Task 3. Create bastion host

- Create a bastion host with two network interfaces, one connected to `griffin-dev-mgmt` and the other connected to `griffin-prod-mgmt`. Make sure you can SSH to the host.

##### GUI path
###### hosts
1. In the Cloud Console, navigate to **Navigation menu** > **Compute Engine** > **VM instances**.
2. Configure the following settings to create the bastion host
Name:- `griffin-dev-db`
Region:- `us-east1`
- Expand the Management, security section.
- Now Add bastion to the Network tags field.
- Click Create.
- Click Add network interface, make sure that you set up two network interfaces,
`griffin-dev-mgmt`
`griffin-prod-mgmt`

###### 2 firewall ssh connection
`allow-bastion-dev-ssh`
- In the GCP Console go to Navigation Menu >VPC Network > Firewall.
- Click Create firewall rule.
- Configure the following settings:
Name:-`allow-bastion-dev-ssh`
Network:-`griffin-dev-vpc`
Targets:-all instance in the network
Source IP ranges:-`192.168.32.0/20`
Protocols and ports:- tcp: `22`
- Click Create.

VPC Network for `allow-bastion-prod-ssh`
- In the GCP Console go to Navigation Menu >VPC Network > Firewall.
- Click Create firewall rule.
- Configure the following settings:
Name:-`allow-bastion-prod-ssh`
Network:-`griffin-dev-vpc`
Targets:-all instance in the network
Source IP ranges:-`192.168.32.0/20`
Protocols and ports:- tcp: `22`
- Click Create.
![](https://miro.medium.com/v2/resize:fit:700/1*Uq8O6OlA5XR4tivQCKyjVw.png)

##### Console Path
```sh
gcloud compute instances create bastion --network-interface=network=griffin-dev-vpc,subnet=griffin-dev-mgmt --network-interface=network=griffin-prod-vpc,subnet=griffin-prod-mgmt --tags=ssh --zone=us-east1-b

gcloud compute firewall-rules create fw-ssh-dev --source-ranges=0.0.0.0/0 --target-tags ssh --allow=tcp:22 --network=griffin-dev-vpc

gcloud compute firewall-rules create fw-ssh-prod --source-ranges=0.0.0.0/0 --target-tags ssh --allow=tcp:22 --network=griffin-prod-vpc
```


## Task 4. Create and configure Cloud SQL Instance

1. Create a **MySQL Cloud SQL Instance** called `griffin-dev-db` in us-east1.
2. Connect to the instance and run the following SQL commands to prepare the **WordPress** environment:
``` mysql
CREATE DATABASE wordpress;
CREATE USER "wp_user"@"%" IDENTIFIED BY "stormwind_rules";
GRANT ALL PRIVILEGES ON wordpress.* TO "wp_user"@"%";
FLUSH PRIVILEGES;
```
These SQL statements create the worpdress database and create a user with access to the wordpress database.
You will use the username and password in task 6.

##### GUI Path
In the console, select **Navigation menu** > **SQL**.
1. Click **CREATE INSTANCE > Choose MySQL** .
2. Use the following parameters to create the instance:
Field- Value
Name:- `griffin-dev-db`
Region:- `us-east1`
Zone:- `us-east1-b`
Root password:- e.g. Mayank@1234 ?
- Click Create.
![](https://miro.medium.com/v2/resize:fit:700/1*mliDxGltmDUesNAzKMImDQ.png)
- Click on the `griffin-dev-db`
- Under Connect to this instance, click on Connect using Cloud Shell.
![](https://miro.medium.com/v2/resize:fit:700/1*AveMk9gCCRFr_Mj9RzovqQ.png)
- Run the commands in Cloud Shell:
`gcloud sql connect griffin-dev-db --user=root --quiet`
- Enter the Root password written in the previous step.
- In the SQL console, run the following query to create the WordPress database:
```mysql
CREATE DATABASE wordpress;  
   GRANT ALL PRIVILEGES ON wordpress.* TO "wp_user"@"%" IDENTIFIED BY "stormwind_rules";  
   FLUSH PRIVILEGES;
exit
```

##### Console Path
```sh
gcloud sql instances create griffin-dev-db --root-password password --region=us-east1
gcloud sql connect griffin-dev-db
```
```mysql
CREATE DATABASE wordpress;
GRANT ALL PRIVILEGES ON wordpress.* TO "wp_user"@"%" IDENTIFIED BY "stormwind_rules";
FLUSH PRIVILEGES;

exit
```

## Task 5. Create Kubernetes cluster

- Create a 2 node cluster (n1-standard-4) called `griffin-dev`, in the `griffin-dev-wp` subnet, and in zone `us-east1-b`.

##### GUI Path
- In the GCP Console go to Navigation Menu > Kubernetes Engine > Clusters.
- Click Create cluster.
- In the Cluster basics tab, configure:
Name: `griffin-dev`  
Zone: `us-east1-b`
![](https://miro.medium.com/v2/resize:fit:700/1*a0FJxaUkknUsJr-bcdfSrA.png)
- Click default-pool under NODE POOLS and set Number of nodes: `2`
- Click Nodes Under default-pool, and set Machine type: `n1-standard-4`
![](https://miro.medium.com/v2/resize:fit:700/1*ZYUy2KfB9M4conkQyJ412A.png)
- Go to the Network tab, set
Network: `griffin-dev-vpc`  
Node subnet: `griffin-dev-wp`
![](https://miro.medium.com/v2/resize:fit:700/1*E-dr84BF-WzNgYuw1Td8BQ.png)
- Click Create.

##### Console Path
```sh
gcloud container clusters create griffin-dev \
 --network griffin-dev-vpc \
 --subnetwork griffin-dev-wp \
 --machine-type n1-standard-4 \
 --num-nodes 2 \
 --zone us-east1-b
-------------------------------------------------------------------------
gcloud container clusters get-credentials griffin-dev --zone us-east1-b
-------------------------------------------------------------------------
cd ~/
gsutil cp -r gs://cloud-training/gsp321/wp-k8s .
-------------------------------------------------------------------------
```

## Task 6. Prepare the Kubernetes cluster
1. Use Cloud Shell and copy all files from `gs://cloud-training/gsp321/wp-k8s`.
The **WordPress** server needs to access the MySQL database using the _username_ and _password_ you created in task 4.
2. You do this by setting the values as secrets. **WordPress** also needs to store its working files outside the container, so you need to create a volume.
3. Add the following secrets and volume to the cluster using `wp-env.yaml`.
4. Make sure you configure the _username_ to `wp_user` and _password_ to `stormwind_rules` before creating the configuration.
You also need to provide a key for a service account that was already set up. This service account provides access to the database for a sidecar container.
5. Use the command below to create the key, and then add the key to the Kubernetes environment:
```sh
gcloud iam service-accounts keys create key.json \
    --iam-account=cloud-sql-proxy@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com
kubectl create secret generic cloudsql-instance-credentials \
    --from-file key.json
```

##### Shell 6,7,8 partial
``` sh
gsutil cp -r gs://cloud-training/gsp321/wp-k8s .
cd wp-k8s
sed -i s/username_goes_here/wp_user/g wp-env.yaml
sed -i s/password_goes_here/stormwind_rules/g wp-env.yaml

----------------------------------------------------------------------------------

kubectl create -f wp-env.yaml
gcloud iam service-accounts keys create key.json --iam-account=cloud-sql-proxy@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com
kubectl create secret generic cloudsql-instance-credentials --from-file key.json

----------------------------------------------------------------------------------

I=$(gcloud sql instances describe griffin-dev-db --format="value(connectionName)")
sed -i s/YOUR_SQL_INSTANCE/$I/g wp-deployment.yaml
kubectl create -f wp-deployment.yaml
kubectl create -f wp-service.yaml
```

##### gui
```sh
gsutil cp -r gs://cloud-training/gsp321/wp-k8s ~/
```
- Edit the YAML file
```sh
cd ~/wp-k8s  
edit wp-env.yaml
```
- Replace `username_goes_here` and `password_goes_here` to `wp_user` and `stormwind_rules.`
- Save the changes.
- Connect the Kubernetes cluster
```sh
gcloud container clusters get-credentials griffin-dev --zone=us-east1
```
- Deploy the configuration to the cluster using.
```sh
kubectl apply -f wp-env.yaml
```
- Create the key, and then add the key to the Kubernetes environment:
```sh
gcloud iam service-accounts keys create key.json \  
       --iam-account=cloud-sql-proxy@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com  
kubectl create secret generic cloudsql-instance-credentials \  
       --from-file key.json
```

## Task 7. Create a WordPress deployment

Now that you have provisioned the MySQL database, and set up the secrets and volume, you can create the deployment using `wp-deployment.yaml`.
1. Before you create the deployment you need to edit `wp-deployment.yaml`.
2. Replace **YOUR_SQL_INSTANCE** with griffin-dev-db's **Instance connection name**.
3. Get the **Instance connection name** from your Cloud SQL instance.
4. After you create your WordPress deployment, create the service with `wp-service.yaml`.
5. Once the Load Balancer is created, you can visit the site and ensure you see the **WordPress** site installer.  
    At this point the dev team will take over and complete the install and you move on to the next task.

- In the Cloud, Shell open the WordPress deployment YAML file.
```sh
cd ~/wp-k8s  
edit wp-deployment.yaml
```
- Replace `YOUR_SQL_INSTANCE` with `griffin-dev-db`’s Instance connection name.
![](https://miro.medium.com/v2/resize:fit:576/1*po7cu_Im1bj5FATkkQY-Ow.png)
- Save the file.
- Now run Deployment and service file in Cloud Shell.
```sh
kubectl create -f wp-deployment.yaml  
kubectl create -f wp-service.yaml
```

## Task 8. Enable monitoring
- Create an uptime check for your WordPress development site.

- In the GCP Console go to Navigation Menu > Monitoring.
- Click Uptime checks.
- Configure using the following parameters:

Field- Value
Title -`WordPress Uptime`
Check Type -`HTTP`
Resource Type -`URL`
Hostname- `YOUR-WORDPRESS_ENDPOINT`
Path- `/`
![](https://miro.medium.com/v2/resize:fit:507/1*n5XsmaklBPw6ZxXYE2Pudw.png)
- Click on Test and Save after that.

## Task 9. Provide access for an additional engineer
- You have an additional engineer starting and you want to ensure they have access to the project, so please go ahead and grant them the editor role to the project.
The second user account for the lab represents the additional engineer.

This step is to Provide access for Second User.
- In the GCP Console go to Navigation Menu > IAM & Admin > IAM.
- Click +ADD.
- Copy and paste the second user account for the lab to the New members field.
- In the Role dropdown, select Project > Editor.
- Click Save.

![](https://miro.medium.com/v2/resize:fit:690/1*RdVNbvNLEYUiNtj1i2_3oQ.png)



```
====================== TASK 8: Enable monitoring ======================

// Navigation Menu -> Kubernetes Engine -> Services and Ingress -> Copy Endpoint's address.

// Navigation Menu -> Monitoring -> Uptime Checks -> + CREATE UPTIME CHECK Title : Wordpress Uptime 
// Next -> Target Hostname : {Endpoint's address} (without http...) Path : / // Next -> Next -> Create


====================== TASK 9: Provide access for an additional engineer ======================

// Navigation Menu -> IAM & Admin -> IAM -> ADD New Member : {Username 2 from Lab instruction page} Role : Project -> Editor

// Save.
```




